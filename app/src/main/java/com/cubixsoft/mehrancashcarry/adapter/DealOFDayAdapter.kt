package com.cubixsoft.mehrancashcarry.adapter
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.mehrancashcarry.MainActivity
import com.cubixsoft.mehrancashcarry.R
import com.cubixsoft.mehrancashcarry.models.ItemsViewModel

class DealOFDayAdapter(private val context: Context, private val raceModel: List<ItemsViewModel>?) :
    RecyclerView.Adapter<DealOFDayAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.tv_name_product)
        val price: TextView = itemView.findViewById(R.id.price)
        val oldPrice: TextView = itemView.findViewById(R.id.oldPrice)
        val image: ImageView = itemView.findViewById(R.id.imageView)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.row_deal_of_day, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel?.get(position)
        if (model != null) {


            holder.title.text = model.text
            holder.oldPrice.setText("97")
            holder.oldPrice.setPaintFlags(holder.oldPrice.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)
            holder.image.setBackgroundResource(model.image)
//            val imageurl: String = model.image
//            if (!TextUtils.isEmpty(imageurl)) {
//                holder.imageLoading.setVisibility(View.VISIBLE)
//                Glide.with(context).load(imageurl)
//                    .listener(object : RequestListener<Drawable?> {
//                        override fun onLoadFailed(
//                            e: GlideException?,
//                            model: Any,
//                            target: Target<Drawable?>,
//                            isFirstResource: Boolean
//                        ): Boolean {
//                            holder.imageLoading.setVisibility(View.GONE)
//                            return false
//                        }
//
//                        override fun onResourceReady(
//                            resource: Drawable?,
//                            model: Any,
//                            target: Target<Drawable?>,
//                            dataSource: DataSource,
//                            isFirstResource: Boolean
//                        ): Boolean {
//                            holder.imageLoading.setVisibility(View.GONE)
//                            return false
//                        }
//                    }).into(holder.image)
////                Picasso.get().load(memberListModels.get(position).getImage()).placeholder(R.drawable.placeholder).into(holder1.ivImage);
//            }
            // sets the image to the imageview from our itemHolder class
//            Glide.with(context).load(model.image).into(holder.image)

        }




        holder.itemView.setOnClickListener {
//            Utilities.saveString(context,"categoryId", model!!.id.toString())
//            Utilities.saveString(context,"title", model.name.toString())
//            Utilities.saveInt(context, "position1", position)
//            Utilities.setPastBookingModels(context,raceModel)
//            Utilities.setPickUpPoints(context, model?.pickupPointsModel)
            (context as MainActivity).navController.navigate(R.id.action_homeFragment_to_addCartFragment)
//            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_categoryFragment)

        }

    }


    override fun getItemCount() = raceModel!!.size

}
