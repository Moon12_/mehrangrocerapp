package com.cubixsoft.mehrancashcarry.adapter
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.mehrancashcarry.CartClickListner
import com.cubixsoft.mehrancashcarry.R
import com.cubixsoft.mehrancashcarry.activities.CartActivity
import com.cubixsoft.mehrancashcarry.models.ItemsViewModel

class CartAdapter(private val raceModel: List<ItemsViewModel>?, cartClickListner: CartActivity) :
    RecyclerView.Adapter<CartAdapter.ViewHolder>() {
    var number = 0
    private val cartClickListner: CartClickListner? = null

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.tv_name)
        val tv_Price: TextView = itemView.findViewById(R.id.tv_Price)
        val tv_quantity: TextView = itemView.findViewById(R.id.tv_quantity)
        val btn_minus: LinearLayout = itemView.findViewById(R.id.btn_minus)
        val btn_add: ImageView = itemView.findViewById(R.id.btn_add)
        val ivCross: ImageView = itemView.findViewById(R.id.ivCross)
//        val image: ImageView = itemView.findViewById(R.id.imageView)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.single_cart_layout, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel?.get(position)
        if (model != null) {


            holder.title.text = model.text
        }

//        val count: Int = carts.size

//        Utilities.saveInt(context, "TotalCartItem", count)

        holder.ivCross.setOnClickListener {
            val newPosition: Int = holder.getAdapterPosition()
//            raceModel.remove(newPosition)
            notifyItemRemoved(newPosition)
            notifyItemRangeChanged(newPosition, raceModel!!.size)
        }
        holder.btn_minus.setOnClickListener(View.OnClickListener {
            number = holder.tv_quantity.getText().toString().toInt()
            if (number <= 1) {
                number = 1
            } else {
                number--
                holder.tv_quantity.setText("" + number)
            }
            val price: String = holder.tv_Price.text.toString();
            val value1 = if (price.isEmpty()) 0.0 else price.toDouble()
            val quantity: String = holder.tv_quantity.getText().toString()
            val value2 = if (quantity.isEmpty()) 0.0 else quantity.toDouble()
            val a: Double
            a = value1 * value2
            val finalValue = java.lang.Double.toString(a)
            holder.tv_Price.setText(finalValue)
//            myAppDatabase.cartDao().update(quantity, carts.get(position).getId())
            if (cartClickListner != null) {
                cartClickListner.onMinusClick()
            }
        })

        holder.btn_add.setOnClickListener(View.OnClickListener {
            number = holder.tv_quantity.getText().toString().toInt()
            number++
            holder.tv_quantity.setText("" + number)
            val price: String = holder.tv_Price.text.toString()
            val value1 = if (price.isEmpty()) 0.0 else price.toDouble()
            val quantity: String = holder.tv_quantity.getText().toString()
            val value2 = if (quantity.isEmpty()) 0.0 else quantity.toDouble()
            val a: Double
            a = value1 * value2
            val finalValue = java.lang.Double.toString(a)
            holder.tv_Price.setText(finalValue)
//            myAppDatabase.cartDao().update(quantity, carts.get(position).getId())
            cartClickListner?.onPlusClick()
        })



        holder.itemView.setOnClickListener {
//            Utilities.saveString(context,"categoryId", model!!.id.toString())
//            Utilities.saveString(context,"title", model.name.toString())
//            Utilities.saveInt(context, "position1", position)
//            Utilities.setPastBookingModels(context,raceModel)
//            Utilities.setPickUpPoints(context, model?.pickupPointsModel)
//            (context as MainActivity).navController.navigate(R.id.action_homeFragment_to_categoriesFragment)
//            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_categoriesFragment)

        }

    }


    override fun getItemCount() = raceModel!!.size

}
