package com.cubixsoft.mehrancashcarry.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.cubixsoft.mehrancashcarry.R
import com.cubixsoft.mehrancashcarry.models.HomePagerModel
import com.makeramen.roundedimageview.RoundedImageView
import com.wang.avi.AVLoadingIndicatorView
import libs.mjn.prettydialog.BuildConfig
import java.util.ArrayList

class ImageSliderAdapter(
    private val context: Context,
    list: ArrayList<HomePagerModel>,
    viewPager2: ViewPager2
) :
    RecyclerView.Adapter<ImageSliderAdapter.ViewHolder>() {
    private val list: ArrayList<HomePagerModel>
    var viewPager2: ViewPager2
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v: View = LayoutInflater.from(context)
            .inflate(R.layout.design_image_slider_container, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model: HomePagerModel = list[position]
        val imageurl: String = model.image.toString()
        holder.image.setImageResource(model.image)
        holder.llShare.setOnClickListener {
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Courage Fit")
                var shareMessage = "\nLet me recommend you this application\n\n"
                shareMessage =
                    """
                   ${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
                   
                   
                   """.trimIndent()
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                context.startActivity(Intent.createChooser(shareIntent, "choose one"))
            } catch (e: Exception) {
                //e.toString();
            }
        }
        if (position == list.size - 2) {
            viewPager2.post(runnable)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: RoundedImageView
        val  llShare:LinearLayout
        var imageLoading: AVLoadingIndicatorView

        init {
            image = itemView.findViewById(R.id.slider_image)
            llShare = itemView.findViewById(R.id.llShare)
            imageLoading = itemView.findViewById(R.id.imageLoading)
        }
    }

    private val runnable = Runnable {
        list.addAll(list)
        notifyDataSetChanged()
    }

    init {
        this.list = list
        this.viewPager2 = viewPager2
    }
}