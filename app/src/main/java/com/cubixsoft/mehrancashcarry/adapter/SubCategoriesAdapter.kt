package com.cubixsoft.mehrancashcarry.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.mehrancashcarry.R
import com.cubixsoft.mehrancashcarry.Utilities
import com.cubixsoft.mehrancashcarry.models.ItemsViewModel

class SubCategoriesAdapter(
    private val context: Context,
    private val raceModel: List<ItemsViewModel>?
) :
    RecyclerView.Adapter<SubCategoriesAdapter.ViewHolder>() {
    private var selectedPosition = -1

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvText: TextView = itemView.findViewById(R.id.tvName)
        val llTop: LinearLayout = itemView.findViewById(R.id.llTop)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.row_subcategory, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel?.get(position)
        if (model != null) {


            holder.tvText.text = model.text
            if (position == 0) {
                holder.tvText.setTextColor(
                    ContextCompat.getColor(
                        holder.tvText.getContext(),
                        R.color.black
                    )
                )
                holder.llTop.setBackgroundDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.bg_et_rounded_carner
                    )
                )
            } else {

                if (selectedPosition == position) {
                    holder.llTop.isSelected = true //using selector drawable
                    holder.tvText.setTextColor(
                        ContextCompat.getColor(
                            holder.tvText.getContext(),
                            R.color.white
                        )
                    )
//            holder.imagebg.visibility = View.GONE
//            holder.imgSELECT.visibility = View.VISIBLE
                    holder.llTop.setBackgroundDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.round_corner_shape_primary_color
                        )
                    )


                } else {
                    holder.llTop.isSelected = false
                    holder.tvText.setTextColor(
                        ContextCompat.getColor(
                            holder.tvText.getContext(),
                            R.color.black
                        )
                    )
//            holder.imagebg.visibility = View.VISIBLE
//            holder.imgSELECT.visibility = View.GONE
                    holder.llTop.setBackgroundDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.round_corner_shape
                        )
                    )


                }
            }



            holder.llTop.setOnClickListener { v ->
                if (selectedPosition >= 0) notifyItemChanged(selectedPosition)
                selectedPosition = holder.adapterPosition
                notifyItemChanged(selectedPosition)
//                Utilities.saveString(context, "pickLocation", model.pickup_point)
            }
        }
    }


    override fun getItemCount() = raceModel!!.size

}
