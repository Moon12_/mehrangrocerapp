package com.cubixsoft.mehrancashcarry.adapter
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.mehrancashcarry.MainActivity
import com.cubixsoft.mehrancashcarry.R
import com.cubixsoft.mehrancashcarry.models.ItemsViewModel
import de.hdodenhof.circleimageview.CircleImageView

class NotificationsAdapters(private val context: Context, private val raceModel: List<ItemsViewModel>?) :
    RecyclerView.Adapter<NotificationsAdapters.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.notificationText)
        val tvDate: TextView = itemView.findViewById(R.id.tvDate)
        val tvNotificationFrom: TextView = itemView.findViewById(R.id.tvNotificationFrom)
        val image: CircleImageView = itemView.findViewById(R.id.profile_image)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.row_notification_item, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel?.get(position)
        if (model != null) {
            holder.title.text = model.text
//            holder.image.setBackgroundResource(model.image)
//            val imageurl: String = model.image
//            if (!TextUtils.isEmpty(imageurl)) {
//                holder.imageLoading.setVisibility(View.VISIBLE)
//                Glide.with(context).load(imageurl)
//                    .listener(object : RequestListener<Drawable?> {
//                        override fun onLoadFailed(
//                            e: GlideException?,
//                            model: Any,
//                            target: Target<Drawable?>,
//                            isFirstResource: Boolean
//                        ): Boolean {
//                            holder.imageLoading.setVisibility(View.GONE)
//                            return false
//                        }
//
//                        override fun onResourceReady(
//                            resource: Drawable?,
//                            model: Any,
//                            target: Target<Drawable?>,
//                            dataSource: DataSource,
//                            isFirstResource: Boolean
//                        ): Boolean {
//                            holder.imageLoading.setVisibility(View.GONE)
//                            return false
//                        }
//                    }).into(holder.image)
////                Picasso.get().load(memberListModels.get(position).getImage()).placeholder(R.drawable.placeholder).into(holder1.ivImage);
//            }
            // sets the image to the imageview from our itemHolder class
//            Glide.with(context).load(model.image).into(holder.image)

        }

        holder.itemView.setOnClickListener {
//            Utilities.saveString(context,"categoryId", model!!.id.toString())
//            Utilities.saveString(context,"title", model.name.toString())
//            Utilities.saveInt(context, "position1", position)
//            Utilities.setPastBookingModels(context,raceModel)
//            Utilities.setPickUpPoints(context, model?.pickupPointsModel)
//            (context as MainActivity).navController.navigate(R.id.action_categoriesFragment_to_addCartFragment)
//            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_categoryFragment)

        }

    }


    override fun getItemCount() = raceModel!!.size

}
