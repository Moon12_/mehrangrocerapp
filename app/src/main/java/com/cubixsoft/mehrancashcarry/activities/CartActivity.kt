package com.cubixsoft.mehrancashcarry.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.mehrancashcarry.R
import com.cubixsoft.mehrancashcarry.adapter.CartAdapter
import com.cubixsoft.mehrancashcarry.base.BaseActivityWithoutVM
import com.cubixsoft.mehrancashcarry.databinding.ActivityCartBinding
import com.cubixsoft.mehrancashcarry.models.ItemsViewModel
import java.util.ArrayList


class CartActivity : BaseActivityWithoutVM<ActivityCartBinding>() {
    override fun getViewBinding(): ActivityCartBinding =
        ActivityCartBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        mViewBinding.apply {
            bottomLayoutCart.setOnClickListener {
                Toast.makeText(this@CartActivity,"click",Toast.LENGTH_LONG).show()
                startActivity(Intent(this@CartActivity, OrderPayActivity::class.java))
            }

        }

        val recyclerview = findViewById<RecyclerView>(R.id.cart_recyclerview)

        // this creates a vertical layout Manager
        recyclerview.layoutManager = LinearLayoutManager(this)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        for (i in 1..3) {
            data.add(ItemsViewModel(R.drawable.kadoo, "Item " + i))
        }

        // This will pass the ArrayList to our Adapter
        val adapter = CartAdapter(data, this)

        // Setting the Adapter with the recyclerview
        recyclerview.adapter = adapter
    }
}