package com.cubixsoft.mehrancashcarry.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cubixsoft.mehrancashcarry.R
import com.cubixsoft.mehrancashcarry.base.BaseActivityWithoutVM
import com.cubixsoft.mehrancashcarry.databinding.ActivityCartBinding
import com.cubixsoft.mehrancashcarry.databinding.ActivityOrderPayBinding

class OrderPayActivity : BaseActivityWithoutVM<ActivityOrderPayBinding>() {
    override fun getViewBinding(): ActivityOrderPayBinding =
        ActivityOrderPayBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_pay)
        mViewBinding.apply {

        }
    }
}