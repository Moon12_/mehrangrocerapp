package com.cubixsoft.mehrancashcarry.activities

import android.content.Intent
import android.os.Bundle
import com.cubixsoft.mehrancashcarry.base.BaseActivityWithoutVM
import com.cubixsoft.mehrancashcarry.databinding.ActivityEnterPhoneBinding
import com.cubixsoft.mehrancashcarry.services.ApiClient

class EnterPhoneActivity : BaseActivityWithoutVM<ActivityEnterPhoneBinding>() {

    private lateinit var apiClient: ApiClient
    private lateinit var newToken: String
    lateinit var loginStatus: String
    override fun getViewBinding(): ActivityEnterPhoneBinding =
        ActivityEnterPhoneBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)

        mViewBinding.apply {
            bContinue.setOnClickListener {

                val intent = Intent(this@EnterPhoneActivity, SignupActivity::class.java)
                startActivity(intent)
            }
        }
    }
}