package com.cubixsoft.mehrancashcarry.activities

import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.cubixsoft.mehrancashcarry.R
import com.github.paolorotolo.appintro.AppIntro
import com.github.paolorotolo.appintro.AppIntroFragment

class AppIntroActivity : AppIntro() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addSlide(
            AppIntroFragment.newInstance(
                "Location", "Define Location for your space...",
                R.drawable.cosmatic, ContextCompat.getColor(applicationContext, R.color.intro1)
            )
        )

        addSlide(
            AppIntroFragment.newInstance(
                "Price", "Define your price range and search fiters...",
                R.drawable.food, ContextCompat.getColor(applicationContext, R.color.intro2)
            )
        )

        addSlide(
            AppIntroFragment.newInstance(
                "Space", "Find your space on map or list...\nLets go!!",
                R.drawable.beverage, ContextCompat.getColor(applicationContext, R.color.intro3)
            )
        )

        addSlide(
            AppIntroFragment.newInstance(
                "Needs", "What do you need in your space?",
                R.drawable.cosmatic, ContextCompat.getColor(applicationContext, R.color.intro4)
            )
        )

        addSlide(
            AppIntroFragment.newInstance(
                "Best Providers",
                "Find and quote with the best providers of professional services...",
                R.drawable.food, ContextCompat.getColor(applicationContext, R.color.intro5)
            )
        )

        addSlide(
            AppIntroFragment.newInstance(
                "Solutions",
                "Find the best solution available in your area efficiently and easily !",
                R.drawable.beverage, ContextCompat.getColor(applicationContext, R.color.intro6)
            )
        )
    }
    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        val intent = Intent(applicationContext, EnterPhoneActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        val intent = Intent(applicationContext, EnterPhoneActivity::class.java)
        startActivity(intent)
        finish()
    }
}