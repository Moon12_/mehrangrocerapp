package com.cubixsoft.mehrancashcarry

interface CartClickListner {

    fun onPlusClick()
    fun onMinusClick()
    fun onDeleteClick()

}