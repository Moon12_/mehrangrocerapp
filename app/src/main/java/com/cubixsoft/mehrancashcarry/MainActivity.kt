package com.cubixsoft.mehrancashcarry

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.cubixsoft.mehrancashcarry.activities.SplashActivity
import com.cubixsoft.mehrancashcarry.base.BaseActivityWithoutVM
import com.cubixsoft.mehrancashcarry.databinding.ActivityMainNewBinding
import com.google.android.material.navigation.NavigationView
import libs.mjn.prettydialog.BuildConfig
import libs.mjn.prettydialog.PrettyDialog
import libs.mjn.prettydialog.PrettyDialogCallback

class MainActivity : BaseActivityWithoutVM<ActivityMainNewBinding>() {
    private var drawer: DrawerLayout? = null
    var logo: ImageView? = null
    lateinit var navController: NavController
    var navigationView: NavigationView? = null
    private val RC_LOCATION_REQUEST = 1234
    var isChange = false
    private val END_SCALE = 0.7f
    override fun getViewBinding(): ActivityMainNewBinding =
        ActivityMainNewBinding.inflate(layoutInflater)

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("UseCompatLoadingForDrawables")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        navigationView = findViewById(R.id.nav_view)
        drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
//        navController = findNavController(R.id.fragment)
        navController = Navigation.findNavController(this, R.id.fragment)

        mViewBinding.apply {
            navView.setupWithNavController(navController)
            bottomNavigation.setupWithNavController(navController)
            val mDrawerToggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(
                this@MainActivity,
                drawer,
                R.string.open,
                R.string.closed
            ) {
                override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                    // Scale the View based on current slide offset
                    val diffScaledOffset: Float =
                        slideOffset * (1 - END_SCALE)
                    val offsetScale = 1 - diffScaledOffset
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        mainContainer.setScaleX(offsetScale)
                        mainContainer.setScaleY(offsetScale)
                    }


                    // Translate the View, accounting for the scaled width
                    val xOffset = drawerView.width * slideOffset
                    val yOffset = drawerView.height * slideOffset
                    val xOffsetDiff: Float = mainContainer.getWidth() * diffScaledOffset / 2
                    val xTranslation = xOffset - xOffsetDiff
                    val yOffsetDiff: Float = mainContainer.getHeight() * diffScaledOffset / 15
                    val yTranslation = yOffset - yOffsetDiff
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        mainContainer.setTranslationX(xTranslation)
                    }
                }
            }

            drawer!!.addDrawerListener(mDrawerToggle)

            drawer!!.setScrimColor(resources.getColor(android.R.color.transparent))

            /*Remove navigation drawer shadow/fadding*/

            /*Remove navigation drawer shadow/fadding*/drawer!!.drawerElevation = 0f
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawer!!.elevation = 0f
                mainContainer.setElevation(10.0f)
            }
       ivNotifications.setOnClickListener {
           Navigation.findNavController(this@MainActivity, R.id.fragment)
               .navigate(R.id.notificationsFragment)
       }

            ivMenu.setOnClickListener(View.OnClickListener {
                drawer!!.openDrawer(
                    GravityCompat.START,
                    true
                )
            })
//            NavigationUI.setupWithNavController(navigationView!!, navController!!)
            navController.addOnDestinationChangedListener { controller, destination, arguments ->
                if (destination.label != null) {
                    if (destination.label!!.equals("HomeFragment")) {
                        rlTop.visibility = View.VISIBLE
                        tvTitle.text = destination.label
                    } else {
                        rlTop.visibility = View.GONE

                    }


                }
            }
            navigationView!!.menu.findItem(R.id.logout).setOnMenuItemClickListener {
                drawer!!.closeDrawer(GravityCompat.START, false)
                showCustomDialog()
                true
            }
            navigationView!!.menu.findItem(R.id.calculate).setOnMenuItemClickListener {
                drawer!!.closeDrawer(GravityCompat.START, false)
                true
            }

//            navigationView!!.menu.findItem(R.id.articles).setOnMenuItemClickListener {
//                drawer!!.closeDrawer(GravityCompat.START, false)
//                Navigation.findNavController(this@MainActivity, R.id.fragment)
//                    .navigate(R.id.articlesFragment)
//                true
//            }
//            navigationView!!.menu.findItem(R.id.tips).setOnMenuItemClickListener {
//                drawer!!.closeDrawer(GravityCompat.START, false)
//                Navigation.findNavController(this@MainActivity, R.id.fragment)
//                    .navigate(R.id.tipsForHealthLifeFragment)
//                true
//            }

            navigationView!!.menu.findItem(R.id.shareApp).setOnMenuItemClickListener {
                drawer!!.closeDrawer(GravityCompat.START, false)
                try {
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "text/plain"
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Health & Nutritions")
                    var shareMessage = "\nLet me recommend you this application\n\n"
                    shareMessage =
                        """
                   ${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
                   
                   
                   """.trimIndent()
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                    startActivity(Intent.createChooser(shareIntent, "choose one"))
                } catch (e: Exception) {
                    //e.toString();
                }
                true
            }


        }

    }

    private fun showCustomDialogforExit() {
        val pDialog = PrettyDialog(this)
        pDialog
            .setTitle("Message")
            .setMessage("Are you sure you want to Exit?")
            .setIcon(R.drawable.pdlg_icon_info)
            .setIconTint(R.color.colorPrimary)
            .addButton(
                "Yes",
                R.color.colorPrimary,
                R.color.pdlg_color_white,
                object : PrettyDialogCallback {
                    override fun onClick() {
                        pDialog.dismiss()
                        finish()
                        finishAffinity()
                    }
                })
            .addButton("No",
                R.color.pdlg_color_red,
                R.color.pdlg_color_white,
                object : PrettyDialogCallback {
                    override fun onClick() {
                        pDialog.dismiss()
                    }
                })
            .show()
    }

    override fun onBackPressed() {
//            super.onBackPressed()
        if (navController.currentDestination
                ?.getLabel().toString() != "Home"
        ) {
            super.onBackPressed()
        } else {
            showCustomDialogforExit()
        }
    }

    private fun showCustomDialog() {
        val pDialog = PrettyDialog(this)
        pDialog
            .setTitle("Message")
            .setMessage("Are you sure you want to Logout?")
            .setIcon(R.drawable.pdlg_icon_info)
            .setIconTint(R.color.color_primary)
            .addButton(
                "Yes",
                R.color.pdlg_color_white,
                R.color.color_primary
            ) {
                Utilities.clearSharedPref(this)
                startActivity(Intent(this, SplashActivity::class.java))
                finish()
                pDialog.dismiss()
            }
            .addButton(
                "No",
                R.color.pdlg_color_white,
                R.color.pdlg_color_red
            ) { pDialog.dismiss() }
            .show()
    }

}