package com.cubixsoft.mehrancashcarry.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.cubixsoft.mehrancashcarry.R
import com.cubixsoft.mehrancashcarry.adapter.*
import com.cubixsoft.mehrancashcarry.databinding.FragmentCategoeiesBinding
import com.cubixsoft.mehrancashcarry.databinding.FragmentHomeBinding
import com.cubixsoft.mehrancashcarry.models.HomePagerModel
import com.cubixsoft.mehrancashcarry.models.ItemsViewModel
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import java.util.ArrayList


class CategoriesFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private lateinit var binding: FragmentCategoeiesBinding
    lateinit var viewPager2: ViewPager2
    private val ImageSliderHandler = Handler()
    var list: ArrayList<HomePagerModel>? = null

    @SuppressLint("UseRequireInsteadOfGet", "ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCategoeiesBinding.inflate(inflater, container, false)
        binding.ivBack.setOnClickListener {
            activity?.onBackPressed()
        }
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Fresh Foods"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Beverage"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Cosmatic"))
//        binding.tabLayout.tabGravity =  binding.tabLayout.GRAVITY_FILL
        binding.tabLayout.setOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                var fragment: Fragment? = null
                when (tab.position) {
                    0 -> Toast.makeText(activity, "O position", Toast.LENGTH_LONG).show()
                    1 -> Toast.makeText(activity, "1 position", Toast.LENGTH_LONG).show()
                    2 -> Toast.makeText(activity, "2 position", Toast.LENGTH_LONG).show()
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })


        // this creates a vertical layout Manager
        binding.rvSubCategory.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        data.add(ItemsViewModel(R.drawable.food, "Vegetables"))
        data.add(ItemsViewModel(R.drawable.beverage, "Fruits "))

        // This will pass the ArrayList to our Adapter
        val adapter = SubCategoriesAdapter(activity!!, data)

        // Setting the Adapter with the recyclerview
        binding.rvSubCategory.adapter = adapter


        binding.productRecyclerview.layoutManager =
            GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data1 = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        data1.add(ItemsViewModel(R.drawable.kadoo, "Pumpkin "))
        data1.add(ItemsViewModel(R.drawable.dhania, " Sabz Dhaniya "))
        data1.add(ItemsViewModel(R.drawable.aloo, "Aloo "))
        data1.add(ItemsViewModel(R.drawable.gajar, "Gajar"))
        data1.add(ItemsViewModel(R.drawable.shimla, "Shimla Mirch "))

        // This will pass the ArrayList to our Adapter
        val adapter1 = ProductsAdapter(activity!!, data1)

        // Setting the Adapter with the recyclerview
        binding.productRecyclerview.adapter = adapter1

        if (!isNetworkAvailable == true) {
            AlertDialog.Builder(activity)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Internet Connection Alert")
                .setMessage("Please Check Your Internet Connection")
                .setPositiveButton(
                    "Retry"
                ) { dialogInterface, i ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        fragmentManager?.beginTransaction()?.detach(this)?.commitNow();
                        fragmentManager?.beginTransaction()?.attach(this)?.commitNow();
                    } else {
                        fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit();
                    }
                }.show()
        } else if (isNetworkAvailable == true) {
            Toast.makeText(
                activity,
                "Welcome", Toast.LENGTH_LONG
            ).show()
        }
        return binding.root
    }

    val isNetworkAvailable: Boolean
        get() {
            val connectivityManager =
                activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                        return true
                    }
                }
            }
            return false
        }

    override fun onRefresh() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fragmentManager?.beginTransaction()?.detach(this)?.commitNow();
            fragmentManager?.beginTransaction()?.attach(this)?.commitNow();
        } else {
            fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit();
        }
    }

}