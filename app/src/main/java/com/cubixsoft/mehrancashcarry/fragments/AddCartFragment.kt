package com.cubixsoft.mehrancashcarry.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.cubixsoft.mehrancashcarry.R
import com.cubixsoft.mehrancashcarry.activities.CartActivity
import com.cubixsoft.mehrancashcarry.databinding.FragmentAddToCartBinding
import com.cubixsoft.mehrancashcarry.models.HomePagerModel
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.models.SlideModel
import java.util.ArrayList


class AddCartFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private lateinit var binding: FragmentAddToCartBinding
    lateinit var viewPager2: ViewPager2
    private val ImageSliderHandler = Handler()
    var list: ArrayList<HomePagerModel>? = null
    var number = 0
    var counter = "1"

    @SuppressLint("UseRequireInsteadOfGet", "ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddToCartBinding.inflate(inflater, container, false)
        val imageList = ArrayList<SlideModel>() // Create image list

// imageList.add(SlideModel("String Url" or R.drawable)
// imageList.add(SlideModel("String Url" or R.drawable, "title") You can add title

        imageList.add(SlideModel(R.drawable.food))
        imageList.add(SlideModel(R.drawable.gajar))
        imageList.add(SlideModel(R.drawable.bags))
        imageList.add(SlideModel(R.drawable.lays))
        imageList.add(SlideModel(R.drawable.cosmatic))

        binding.imageSlider.setImageList(imageList)
        binding.ivBack.setOnClickListener {
            activity?.onBackPressed()
        }


        if (!isNetworkAvailable == true) {
            AlertDialog.Builder(activity)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Internet Connection Alert")
                .setMessage("Please Check Your Internet Connection")
                .setPositiveButton(
                    "Retry"
                ) { dialogInterface, i ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        fragmentManager?.beginTransaction()?.detach(this)?.commitNow();
                        fragmentManager?.beginTransaction()?.attach(this)?.commitNow();
                    } else {
                        fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit();
                    }
                }.show()
        } else if (isNetworkAvailable == true) {
            Toast.makeText(
                activity,
                "Welcome", Toast.LENGTH_LONG
            ).show()
        }


        binding.btnMinusQty.setOnClickListener(View.OnClickListener {
            number = binding.tvTotalCount.getText().toString().toInt()
            if (number <= 1) {
                number = 1
            } else {
                number--
                binding.tvTotalCount.setText("" + number)
            }
            val price: String = binding.tvPriceCart.text.toString()

            val value1 = if (price.isEmpty()) 0.0 else price.toDouble()
            val quantity: String = binding.tvTotalCount.getText().toString()
            val value2 = if (quantity.isEmpty()) 0.0 else quantity.toDouble()
            val a: Double
            a = value1 * value2
            val finalValue = java.lang.Double.toString(a)
            binding.tvTotalPrice.setText(finalValue)


        })
        binding.addtoCartLayout.setOnClickListener {
            startActivity(Intent(activity, CartActivity::class.java))
        }
        binding.btnAddQty.setOnClickListener(View.OnClickListener {
            number = binding.tvTotalCount.getText().toString().toInt()
            number++
            binding.tvTotalCount.setText("" + number)
            counter = binding.tvTotalCount.getText().toString()

            val price: String = binding.tvPriceCart.text.toString()
            val value1 = if (price.isEmpty()) 0.0 else price.toDouble()
            val quantity: String = binding.tvTotalCount.getText().toString()
            val value2 = if (quantity.isEmpty()) 0.0 else quantity.toDouble()
            val a: Double
            a = value1 * value2
            val finalValue = java.lang.Double.toString(a)
            binding.tvTotalPrice.setText(finalValue)


        })
        return binding.root
    }

    val isNetworkAvailable: Boolean
        get() {
            val connectivityManager =
                activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                        return true
                    }
                }
            }
            return false
        }

    override fun onRefresh() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fragmentManager?.beginTransaction()?.detach(this)?.commitNow();
            fragmentManager?.beginTransaction()?.attach(this)?.commitNow();
        } else {
            fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit();
        }
    }

}