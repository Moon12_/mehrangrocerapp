package com.cubixsoft.mehrancashcarry.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.cubixsoft.mehrancashcarry.R
import com.cubixsoft.mehrancashcarry.adapter.CategoryAdapter
import com.cubixsoft.mehrancashcarry.adapter.DealOFDayAdapter
import com.cubixsoft.mehrancashcarry.adapter.ImageSliderAdapter
import com.cubixsoft.mehrancashcarry.databinding.FragmentHomeBinding
import com.cubixsoft.mehrancashcarry.models.HomePagerModel
import com.cubixsoft.mehrancashcarry.models.ItemsViewModel
import java.util.ArrayList


class HomeFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private lateinit var binding: FragmentHomeBinding
    lateinit var viewPager2: ViewPager2
    private val ImageSliderHandler = Handler()
    var list: ArrayList<HomePagerModel>? = null

    @SuppressLint("UseRequireInsteadOfGet", "ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        viewPager2 = binding.viewPager222
        // this creates a vertical layout Manager
        binding.rvDealOfDay.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        data.add(ItemsViewModel(R.drawable.beverage, "Beverage "))
        data.add(ItemsViewModel(R.drawable.food, "Fresh Food "))
        data.add(ItemsViewModel(R.drawable.bags, "Leather Bags "))
        data.add(ItemsViewModel(R.drawable.lays, "Potato Lays "))
        data.add(ItemsViewModel(R.drawable.cosmatic, "Cosmatic "))

        // This will pass the ArrayList to our Adapter
        val adapter = DealOFDayAdapter(activity!!, data)

        // Setting the Adapter with the recyclerview
        binding.rvDealOfDay.adapter = adapter


        binding.rvCATEGORY.layoutManager =
            GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data1 = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        data1.add(ItemsViewModel(R.drawable.beverage, "Beverage "))
        data1.add(ItemsViewModel(R.drawable.food, "Fresh Food "))
        data1.add(ItemsViewModel(R.drawable.bags, "Leather Bags "))
        data1.add(ItemsViewModel(R.drawable.lays, "Potato Lays "))
        data1.add(ItemsViewModel(R.drawable.cosmatic, "Cosmatic "))

        // This will pass the ArrayList to our Adapter
        val adapter1 = CategoryAdapter(activity!!, data1)

        // Setting the Adapter with the recyclerview
        binding.rvCATEGORY.adapter = adapter1


        binding.topFeaturedRecycler.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        // ArrayList of class ItemsViewModel
        val data2 = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        data2.add(ItemsViewModel(R.drawable.beverage, "Beverage "))
        data2.add(ItemsViewModel(R.drawable.food, "Fresh Food "))
        data2.add(ItemsViewModel(R.drawable.bags, "Leather Bags "))
        data2.add(ItemsViewModel(R.drawable.lays, "Potato Lays "))
        data2.add(ItemsViewModel(R.drawable.cosmatic, "Cosmatic "))

        // This will pass the ArrayList to our Adapter
        val adapter12 = DealOFDayAdapter(activity!!, data2)

        // Setting the Adapter with the recyclerview
        binding.topFeaturedRecycler.adapter = adapter12


        setSlider()
        if (!isNetworkAvailable == true) {
            AlertDialog.Builder(activity)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Internet Connection Alert")
                .setMessage("Please Check Your Internet Connection")
                .setPositiveButton(
                    "Retry"
                ) { dialogInterface, i ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        fragmentManager?.beginTransaction()?.detach(this)?.commitNow();
                        fragmentManager?.beginTransaction()?.attach(this)?.commitNow();
                    } else {
                        fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit();
                    }
                }.show()
        } else if (isNetworkAvailable == true) {
            Toast.makeText(
                activity,
                "Welcome", Toast.LENGTH_LONG
            ).show()
        }
        return binding.root
    }

    val isNetworkAvailable: Boolean
        get() {
            val connectivityManager =
                activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                        return true
                    }
                }
            }
            return false
        }

    override fun onRefresh() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fragmentManager?.beginTransaction()?.detach(this)?.commitNow();
            fragmentManager?.beginTransaction()?.attach(this)?.commitNow();
        } else {
            fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit();
        }
    }

    private fun setSlider() {

        //Slider images
        list = ArrayList()
        list!!.add(HomePagerModel(R.drawable.demo3, "ipsum lorem dummy text"))
        list!!.add(HomePagerModel(R.drawable.demo4, "ipsum lorem dummy text"))
        list!!.add(HomePagerModel(R.drawable.demo5, "ipsum lorem dummy text"))
        list!!.add(HomePagerModel(R.drawable.demo1, "ipsum lorem dummy text"))
        list!!.add(HomePagerModel(R.drawable.demo2, "ipsum lorem dummy text"))

        viewPager2.setAdapter(activity?.let { ImageSliderAdapter(it, list!!, viewPager2) })
        viewPager2.setClipToPadding(false)
        viewPager2.setClipChildren(false)
        viewPager2.setOffscreenPageLimit(2)
        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER)
        val compositePageTransformer = CompositePageTransformer()
        compositePageTransformer.addTransformer(MarginPageTransformer(40))
        compositePageTransformer.addTransformer { page, position ->
            val a = 1 - Math.abs(position)
            page.scaleY = 0.85f + a * 0.15f
        }
        viewPager2.setPageTransformer(compositePageTransformer)
        viewPager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                ImageSliderHandler.removeCallbacks(imagesliderRunable)
                ImageSliderHandler.postDelayed(imagesliderRunable, 3000)
            }
        })
    }

    private val imagesliderRunable =
        Runnable { viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1) }
}